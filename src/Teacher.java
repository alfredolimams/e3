import java.util.ArrayList;

public class Teacher extends People {

	private ArrayList<Production> productions;

	
	public Teacher(String name, String cpf, String email) {
		super(name, cpf, email);
		// TODO Auto-generated constructor stub
	}

	public void AddPublications( Production production ){
		productions.add(production);
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		String to = super.toString() + "\n";
		for (int i = 0; i < productions.size() ; i++) {
			to += productions.get(i) + "\n";
		} 
		return to;
	}
}
