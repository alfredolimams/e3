
public class People {

	/**
	 * @param args
	 */
	protected String name;
	protected String cpf;
	protected String email;
	
	public People( String name, String cpf, String email ){
		this.name = name;
		this.cpf = cpf;
		this.email = email;
	}
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		String out = "CPF : 		Nome:			Email:\n";
		out += this.cpf + " " + this.name + " " + this.email;
		return out;
	}
	

}
